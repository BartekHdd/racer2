#ifndef TILE_H
#define TILE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "vec.h"

typedef struct Tile_t {
  Ivec2 top_left, down_right;
} Tile_t;

Tile_t create_tile(Ivec2 top_left, Ivec2 down_right);

int tile_width(Tile_t *tile);
int tile_height(Tile_t *tile);

// returns number of pixels inside a tile
int tile_area(Tile_t *tile);

// returns global coordinates by in-tile index of pixel:
Ivec2 tile_get_coordinates(Tile_t *tile, int pixel_index);

// returns pixel in-tile index by coordinates
int tile_get_index(Tile_t *tile, Ivec2 coordinates);

typedef Vec3 *TileBuffer_t;

TileBuffer_t create_tile_buffer(Tile_t *tile);

void free_tile_buffer(TileBuffer_t);

void set_pixel(Tile_t *tile, TileBuffer_t buffer, Ivec2 coordinates,
               Vec3 color);

void tile_buffer_pattern_fill(Tile_t *tile, TileBuffer_t buffer,
                              Vec3 (*pattern_function)(Ivec2 coordinates));

void bool_flip(bool *variable);
Vec3 checked_pattern(Ivec2 coordinates);
Vec3 noise_pattern(Ivec2 coordinates);

bool tile_to_ppm(Tile_t *tile, TileBuffer_t buffer, char *output_file);
#endif // TILE_H
