#ifndef VEC_H
#define VEC_H

typedef struct Ivec2 {
  int x, y;
} Ivec2;

Ivec2 civec2(int x, int y);

typedef struct Ivec3 {
  int x, y, z;
} Ivec3;

Ivec3 civec3(int x, int y, int z);

typedef struct Vec3 {
  float x, y, z;
} Vec3;

Vec3 cvec3(float x, float y, float z);

Ivec3 vec_to_rgb(Vec3 *vec);

#endif // VEC_H