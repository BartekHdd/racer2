#ifndef RENDER_H
#define RENDER_H

#include "tile.h"

struct Properties {
  Tile_t image;
  TileBuffer_t buffer;
};

struct Properties create_render_properties();

void free_render_properties(struct Properties *render);

#endif // RENDER_H
