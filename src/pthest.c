#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct Data { int x; float y; };

void *funkcja(void *arg) {
	struct Data *value = (struct Data*) arg;
	printf("%i\n", value->x);
	value->x = 5;
	return (void*) 0xC0DE;
}

int main() {
	pthread_t threadID;
	int value = 0;
	void *result;
	struct Data data = { 1, 1.0 };

	printf("%i\n", data.x);
	pthread_create(&threadID, NULL, funkcja, &data);
	pthread_join(threadID, &result);
	printf("%i\n", data.x);
	return 0;
}
