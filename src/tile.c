#include "tile.h"

Tile_t create_tile(Ivec2 top_left, Ivec2 down_right) {
  return (Tile_t){top_left, down_right};
}

int tile_width(Tile_t *tile) {
  return (tile->down_right.x - tile->top_left.x + 1);
}

int tile_height(Tile_t *tile) {
  return (tile->down_right.y - tile->top_left.y + 1);
}

int tile_area(Tile_t *tile) { return tile_width(tile) * tile_height(tile); }

Ivec2 tile_get_coordinates(Tile_t *tile, int pixel_index) {
  int x = (pixel_index - 1) % tile_width(tile) + tile->top_left.x;
  int y = (pixel_index - 1) / tile_width(tile) + tile->top_left.y;
  return civec2(x, y);
}

int tile_get_index(Tile_t *tile, Ivec2 coordinates) {
  return (coordinates.y) * tile_width(tile) + (coordinates.x + 1);
}

TileBuffer_t create_tile_buffer(Tile_t *tile) {
  return (TileBuffer_t)malloc(tile_area(tile) * sizeof(Vec3));
}

void free_tile_buffer(TileBuffer_t buffer) { free(buffer); }

void set_pixel(Tile_t *tile, TileBuffer_t buffer, Ivec2 coordinates,
               Vec3 color) {
  buffer[tile_get_index(tile, coordinates) - 1] = color;
}

void tile_buffer_pattern_fill(Tile_t *tile, TileBuffer_t buffer,
                              Vec3 (*pattern_function)(Ivec2 coordinates)) {
  for (int i = 1; i <= tile_area(tile); i++) {
    Ivec2 coordinates = tile_get_coordinates(tile, i);
    set_pixel(tile, buffer, coordinates, pattern_function(coordinates));
  }
}

void bool_flip(bool *variable) {
  if (*variable == true)
    *variable = false;
  else
    *variable = true;
}

Vec3 checked_pattern(Ivec2 coordinates) {
  bool color;
  if (((coordinates.x) / 13) % 2 == 0)
    color = true;
  else
    color = false;
  if (((coordinates.y) / 13) % 2 == 0)
    bool_flip(&color);
  if (color)
    return cvec3(0.9, 0.9, 0.9);
  else
    return cvec3(0.8, 0.8, 0.8);
}

Vec3 noise_pattern(Ivec2 coordinates) {
  return cvec3(drand48(), drand48(), drand48());
}

bool tile_to_ppm(Tile_t *tile, TileBuffer_t buffer, char *output_file) {
  FILE *output = fopen(output_file, "w");
  fprintf(output, "P3 %i %i 255\n", tile_width(tile), tile_height(tile));
  for (int i = 0; i < tile_area(tile); i++) {
    Ivec3 rgb = vec_to_rgb(&buffer[i]);
    fprintf(output, "%i %i %i\n", rgb.x, rgb.y, rgb.z);
  }
  return 0;
}
