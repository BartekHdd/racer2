#include "vec.h"

Ivec2 civec2(int x, int y) { return (Ivec2){x, y}; }

Ivec3 civec3(int x, int y, int z) { return (Ivec3){x, y, z}; }

Vec3 cvec3(float x, float y, float z) { return (Vec3){x, y, z}; }

Ivec3 vec_to_rgb(Vec3 *vec) {
  int r = (int)(vec->x * 255.99);
  int g = (int)(vec->y * 255.99);
  int b = (int)(vec->z * 255.99);
  return civec3(r, g, b);
}