#include <pthread.h>
#include <stdio.h>

#include "preview.h"
#include "render.h"
#include "scheduler.h"
#include "tile.h"

int main() {
  struct Properties render_properties = create_render_properties();
  struct TileSchedule schedule;
  schedule = gen_tile_schedule_grid(&render_properties.image, 40);
  schedule.main_tile = render_properties.image;
  schedule.buffer = render_properties.buffer;
  // tile_buffer_pattern_fill(&render_properties.image,
  // render_properties.buffer, noise_pattern);

  struct Preview preview_data = new_preview(&schedule);

  pthread_t scheduler, preview;

  pthread_create(&preview, NULL, preview_thread, &preview_data);
  // sleep(5);
  pthread_create(&scheduler, NULL, scheduler_thread, &schedule);
  pthread_join(scheduler, NULL);
  pthread_join(preview, NULL);

  tile_to_ppm(&render_properties.image, render_properties.buffer, "out.ppm");

  quit_preview(&preview_data);
  free_tile_schedule(&schedule);
  free_render_properties(&render_properties);
  return 0;
}
