#include "scheduler.h"

#define THREADS 4 

struct TileSchedule gen_tile_schedule_rows(Tile_t *main_tile) {
  int scheduled = 0;
  int all = tile_height(main_tile);
  bool volatile *complete = (bool volatile *) malloc(all * sizeof(bool));
  Tile_t *tiles = (Tile_t *) malloc(all * sizeof(Tile_t));
  int w = tile_width(main_tile) - 1;
  for (int i = 0; i < all; i++) {
    complete[i] = false;
    tiles[i] = create_tile(civec2(0, i), civec2(w, i));
  }
  for (int i = 0; i < all; i++) {
	  if (complete[i] || tiles[i].top_left.x != 0) {
		  printf("%i: complete %i | [%i, %i] [%i, %i]",
				  i, complete[i], 
				  tiles[i].top_left.x,
				  tiles[i].top_left.y,
				  tiles[i].down_right.x,
				  tiles[i].down_right.y);
	  }
  }
  return (struct TileSchedule){scheduled, all, complete, tiles};
}

struct TileSchedule gen_tile_schedule_grid(Tile_t *main_tile, int tile_size) {
  Tile_t *tiles = (Tile_t *)malloc(sizeof(Tile_t));
  int all = 0;
  
  for (int vertical_n = 1;;vertical_n++) {
    int top = (vertical_n - 1) * tile_size;
    int down = vertical_n * tile_size -1;
    if (top > main_tile->down_right.y) break;
    else if (down > main_tile->down_right.y) down = main_tile->down_right.y;
    for (int horizontal_n = 1;;horizontal_n++) {
      int left = (horizontal_n - 1) * tile_size;
      int right = horizontal_n * tile_size - 1;
      if (left > main_tile->down_right.x) break;
      else if (right > main_tile->down_right.x) right = main_tile->down_right.x;
      tiles = realloc(tiles, (all + 1) * sizeof(Tile_t));
      tiles[all].top_left = civec2(left, top);
      tiles[all].down_right = civec2(right, down);
      all++;
  }
  }
  bool volatile *complete = (bool volatile*) malloc(all*sizeof(bool));
  return (struct TileSchedule) {0, all, complete, tiles};
}

void schedule_print(struct TileSchedule *schedule) {
  while (available_tiles(schedule)) {
    Tile_t t = next_tile(schedule);
    printf("[%i, %i | %i, %i]", t.top_left.x, t.top_left.y, t.down_right.x,
           t.down_right.y);
  }
}

bool available_tiles(struct TileSchedule *schedule) {
  return (schedule->scheduled != schedule->all);
}

Tile_t next_tile(struct TileSchedule *schedule) {
  Tile_t tile = schedule->tiles[schedule->scheduled];
  schedule->scheduled += 1;
  return tile;
}

void free_tile_schedule(struct TileSchedule *schedule) {
  free((bool*) schedule->complete);
  free(schedule->tiles);
}

char *emots[] = {":*", ":)", ":D", ";)", ":O", "xD", ":}", "~[o]^[o]~"};

void *worker_thread(void *worker_handle) {
  struct WorkerHandle *handle = (struct WorkerHandle *)worker_handle;
  printf("Worker thread %i is starting on %ld %s\n", handle->worker_id,
         handle->thread_id, emots[handle->worker_id]);
  while (handle->running) {
    if (handle->working) {
      for (int i = 1; i <= tile_area(&handle->tile); i++) {
        Ivec2 coordinates = tile_get_coordinates(&handle->tile, i);
        set_pixel(&handle->main_tile, handle->buffer, coordinates,
                  cvec3(1, 0, 1));
      }
	//sleep(1);
      handle->working = false;
    }
  }

  // sleep(1);
  printf("Bye worker thread %i\n", handle->worker_id);
  return (void *)0xC0DE;
}

void *scheduler_thread(void *tile_schedule) {
  struct TileSchedule *schedule = (struct TileSchedule *)tile_schedule;
  printf("OMG Tile scheduler has started\n");
  struct WorkerHandle workers[THREADS];
  for (int i = 0; i < THREADS; i++) {
    workers[i].worker_id = i;
    workers[i].running = true;
    workers[i].working = true;
    workers[i].main_tile = schedule->main_tile;
    workers[i].buffer = schedule->buffer;
    workers[i].tile_id = schedule->scheduled;
    if (available_tiles(schedule))
      workers[i].tile = next_tile(schedule);
    else
      workers[i].running = false;
    pthread_create(&workers[i].thread_id, NULL, worker_thread, &workers[i]);
  }
  int active_threads = THREADS;
  while (active_threads) {
    for (int i = 0; i < THREADS; i++) {
      if (workers[i].running && workers[i].working == 0) {
        if (available_tiles(schedule)) {
          schedule->complete[workers[i].tile_id] = true;
          workers[i].tile_id = schedule->scheduled;
          workers[i].tile = next_tile(schedule);
          workers[i].working = true;
        } else {
          schedule->complete[workers[i].tile_id] = true;
          workers[i].running = false;
          active_threads--;
        }
      }
    }
  }
  for (int i = 0; i < THREADS; i++)
    pthread_join(workers->thread_id, NULL);
  printf("Scheduler is leaving\n");
  return (void *)0xC0DE;
}
