#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "tile.h"

struct TileSchedule {
  int scheduled, all;
  bool volatile *complete;
  Tile_t *tiles;
  Tile_t main_tile;
  TileBuffer_t buffer;
};

struct TileSchedule gen_tile_schedule_rows(Tile_t *main_tile);
struct TileSchedule gen_tile_schedule_grid(Tile_t *main_tile, int tile_size);


void schedule_print(struct TileSchedule *schedule);

bool available_tiles(struct TileSchedule *schedule);

Tile_t next_tile(struct TileSchedule *schedule);

void free_tile_schedule(struct TileSchedule *schedule);

struct WorkerHandle {
  int worker_id;
  pthread_t thread_id;
  volatile bool running, working;
  volatile int tile_id;
  Tile_t tile;
  Tile_t main_tile;
  TileBuffer_t buffer;
};

void *worker_thread(void *handle);

void *scheduler_thread(void *tile_schedule);

#endif // SCHEDULER_H
