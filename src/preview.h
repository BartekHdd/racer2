#ifndef PREVIEW_H
#define PREVIEW_H

#include <SDL2/SDL.h>

#include "scheduler.h"
#include "tile.h"
#include "vec.h"

struct Preview {
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Texture *texture;
  struct TileSchedule *scheduler;
};

struct Preview new_preview(struct TileSchedule *scheduler);

void preview_load_tile(struct Preview *preview, Tile_t *tile);

void *preview_thread(void *data);

void quit_preview(struct Preview *preview);

#endif // PREVIEW_H
