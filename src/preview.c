#include "preview.h"

struct Preview new_preview(struct TileSchedule *scheduler) {
  SDL_Init(SDL_INIT_VIDEO);
  struct Preview preview;
  preview.scheduler = scheduler;
  SDL_CreateWindowAndRenderer(tile_width(&scheduler->main_tile),
                              tile_height(&scheduler->main_tile), 0,
                              &preview.window, &preview.renderer);
  preview.texture = SDL_CreateTexture(preview.renderer, 
                SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_TARGET, 
                tile_width(&scheduler->main_tile), tile_height(&scheduler->main_tile));
  preview_load_tile(&preview, &scheduler->main_tile);
  SDL_RenderPresent(preview.renderer);
  return preview;
}

void preview_load_tile(struct Preview *preview, Tile_t *tile) {
  SDL_SetRenderTarget(preview->renderer, preview->texture);
  for (int i = 1; i <= tile_area(tile); i++) {
    Ivec2 coordinates = tile_get_coordinates(tile, i);
    Ivec3 color = vec_to_rgb(
        &preview->scheduler->buffer
             [tile_get_index(&preview->scheduler->main_tile, coordinates) - 1]);
    SDL_SetRenderDrawColor(preview->renderer, color.x, color.y, color.z, 255);
    SDL_RenderDrawPoint(preview->renderer, coordinates.x, coordinates.y);
  }
  SDL_SetRenderTarget(preview->renderer, NULL);
  SDL_RenderCopy(preview->renderer, preview->texture, NULL, NULL);
}

void *preview_thread(void *data) {
  struct Preview *preview = (struct Preview *)data;
  bool loaded[preview->scheduler->all];
  for (int i = 0; i < preview->scheduler->all; i++)
    loaded[i] = false;

  SDL_Event e;
  bool quit = false;

  while (!quit) {
    
    for (int i = 0; i < preview->scheduler->all; i++) {
      if (preview->scheduler->complete[i] && loaded[i] == false) {
        preview_load_tile(preview, &preview->scheduler->tiles[i]);
        loaded[i] = true;
        SDL_RenderPresent(preview->renderer);
      }
    }
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        quit = true;
      }
    }
  }
  return (void *)0xC0DE;
}

void quit_preview(struct Preview *preview) {
  
  SDL_DestroyRenderer(preview->renderer);
  SDL_DestroyWindow(preview->window);
  SDL_Quit();
}