#include "render.h"

struct Properties create_render_properties() {
  Tile_t image = create_tile(civec2(0, 0), civec2(999, 499));
  TileBuffer_t buffer = create_tile_buffer(&image);
  tile_buffer_pattern_fill(&image, buffer, checked_pattern);
  return (struct Properties){image, buffer};
}

void free_render_properties(struct Properties *render) {
  free_tile_buffer(render->buffer);
}
