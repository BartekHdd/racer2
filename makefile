BUILD_DIR ?= ./build
 
SRCS := racer.c tile.c scheduler.c vec.c render.c preview.c
OBJS := ${SRCS:%=$(BUILD_DIR)/%.o}
OBJS := ${OBJS:.c.o=.o}
 
LIBS := -lm -lSDL2 -pthread
FLAGS := -Wall -O3
ASANFLAGS := -fsanitize=address -fno-common -fno-omit-frame-pointer

Racer: $(BUILD_DIR) $(OBJS)
	$(CC) $(OBJS) -o $@ $(LIBS)
	cloc src --hide-rate
	./Racer
	#xdg-open out.ppm

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)
 
$(BUILD_DIR)/%.o: src/%.c
	$(CC) -c $< -o $@ $(FLAGS)
                                                                            
clean:                                                                          
	rm -r $(BUILD_DIR)
